import { Component, OnInit      } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submitted = false;

  constructor() { }

  ngOnInit() { }

  loginUser(formLogin){
    console.log(formLogin);

    return true;
  }


 onSubmit() { this.submitted = true; }

}
