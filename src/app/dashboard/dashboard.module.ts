import { NgModule                } from '@angular/core';
import { CommonModule            } from '@angular/common';

import { UsuarioComponent        } from './usuario/usuario.component';
import { AddSocioComponent       } from './add-socio/add-socio.component';
import { ManipularSocioComponent } from './manipular-socio/manipular-socio.component';

import { UsuarioService          } from './usuario.service';

import { RoutingDashboardModule  } from './routing.dashboard.module';

import { DeactivateGuard        } from './guards/deactivate.guard';
import { PermitionGuard         } from './guards/permission.guard';

@NgModule({
  imports: [
    CommonModule,
    RoutingDashboardModule
  ],
  declarations: [
    UsuarioComponent,
    AddSocioComponent,
    ManipularSocioComponent
  ],
  providers: [
    UsuarioService,
    DeactivateGuard,
    PermitionGuard
  ]
})
export class DashboardModule { }
