import { NgModule                } from '@angular/core';
import { Routes, RouterModule    } from '@angular/router';

import { PermitionGuard          } from './guards/permission.guard';
import { DeactivateGuard         } from './guards/deactivate.guard';

import { AddSocioComponent       } from './add-socio/add-socio.component';
import { ManipularSocioComponent } from './manipular-socio/manipular-socio.component';
import { UsuarioComponent        } from './usuario/usuario.component';

const DASHBOARD_ROUTES: Routes = [
    { path: 'dashboard', component: UsuarioComponent,
        children: [
          { path: 'update-socio',  component: AddSocioComponent  },
          { path: 'add-socio',    component: ManipularSocioComponent }
    ] }
];

@NgModule({
  imports: [RouterModule.forChild(DASHBOARD_ROUTES)],
  exports: [RouterModule]
})

export class RoutingDashboardModule { }
