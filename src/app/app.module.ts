import { BrowserModule         } from '@angular/platform-browser';
import { NgModule              } from '@angular/core';
import { FormsModule           } from '@angular/forms';

import { AppComponent          } from './app.component';
import { RegisterComponent     } from './authentication/register/register.component';
import { LoginComponent        } from './authentication/login/login.component';
import { Erro404Component      } from './http-erros/erro404/erro404.component';
import { HeaderComponent       } from './partls/header/header.component';
import { FooterComponent       } from './partls/footer/footer.component';

import { AuthGuard             } from './guards/authguard.service';
import { AuthenticationService } from './authentication/authentication.service';

import { DashboardModule       } from './dashboard/dashboard.module';
import { RoutingModule         } from './routing.module';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    Erro404Component,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    DashboardModule,
    FormsModule,
    RoutingModule
  ],
  providers: [
    AuthGuard,
    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
