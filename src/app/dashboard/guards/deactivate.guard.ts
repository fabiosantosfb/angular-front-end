import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { Injectable               } from '@angular/core';
import { Observable               } from "rxjs/Rx";

import { InterfaceFormCandeactive } from "../../model/inteface-form-candeactive";

@Injectable()
export class DeactivateGuard implements CanDeactivate<InterfaceFormCandeactive> {

    constructor(private router: Router) { }

    canDeactivate(component: InterfaceFormCandeactive, childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot):Observable<boolean> | boolean {
        return component.deActiviteRouter();
    }
}
