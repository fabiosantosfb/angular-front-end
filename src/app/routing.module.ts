

import { NgModule             } from '@angular/core';
import { CommonModule         } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { LoginComponent    } from './authentication/login/login.component';
import { RegisterComponent } from './authentication/register/register.component';
import { Erro404Component  } from './http-erros/erro404/erro404.component';

import { AuthGuard         } from './guards/authguard.service';

const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent,
      data: { title: 'Login' }
    },
    { path: 'register',  component: RegisterComponent,
      data: { title: 'Registro' }
    },
    { path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        // canActivate: [AuthGuard],
        // canLoad: [AuthGuard],
        data: { title: 'Painel Administrativo' }
    },
    { path: '',  pathMatch: 'full', redirectTo: 'login' },
    { path: '**',       component: Erro404Component,
      data: { title: '404-nofound' }
    }
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class RoutingModule { }
