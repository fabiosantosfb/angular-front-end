import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManipularSocioComponent } from './manipular-socio.component';

describe('ManipularSocioComponent', () => {
  let component: ManipularSocioComponent;
  let fixture: ComponentFixture<ManipularSocioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManipularSocioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManipularSocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
